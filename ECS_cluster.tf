provider "aws" {
  region = "us-west-1"
}

resource "aws_ecs_cluster" "cluster" {
  name = "IDL_CICD_demo"
}

resource "aws_ecs_task_definition" "task" {
  family                = "IDL-Docker-latest"
  container_definitions = file("task-definitions.json")
}

resource "aws_ecs_service" "service" {
  name            = "IDL-Docker-WebsiteCICD"
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.task.arn
  desired_count   = 1
}
