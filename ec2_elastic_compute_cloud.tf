
resource "aws_internet_gateway" "idl" {
  vpc_id = aws_vpc.eman.id
}


resource "aws_instance" "eman-idl-platform" {
  availability_zone = "us-west-1b"
  instance_type = "t2.micro"
  subnet_id  = aws_subnet.subnet_10_10_10.id
  ami="ami-0fd61683ae1a27a64"
  
}
resource "aws_instance" "eman-idl-platform-tst" {
  availability_zone = "us-west-1c"
  instance_type = "t2.micro"
  subnet_id  = aws_subnet.subnet_10_10_11.id
  ami="ami-0fd61683ae1a27a64"
  
  }


resource "aws_eip" "eman_idl_platform" {
  instance                  = aws_instance.eman-idl-platform.id
 
  
  tags = {
    Name                                = "eman-idl-platform"
    "elasticbeanstalk:environment-id"   = "e-be9yphrfpt"
    "elasticbeanstalk:environment-name" = "eman-idl-platform"
  }

  tags_all = {
    Name                                = "eman-idl-platform"
    "elasticbeanstalk:environment-id"   = "e-be9yphrfpt"
    "elasticbeanstalk:environment-name" = "eman-idl-platform"
  }

 
  
}

resource "aws_eip" "eman_idl_platform_tst" {
  instance                  = aws_instance.eman-idl-platform-tst.id
 
  
  
  
  tags = {
    Name                                = "eman-idl-platform-tst"
    "elasticbeanstalk:environment-id"   = "e-3ayamaifnm"
    "elasticbeanstalk:environment-name" = "eman-idl-platform-tst"
  }

  tags_all = {
    Name                                = "eman-idl-platform-tst"
    "elasticbeanstalk:environment-id"   = "e-3ayamaifnm"
    "elasticbeanstalk:environment-name" = "eman-idl-platform-tst"
  }

  
}
