

resource "aws_route_table" "rtb_1" {
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.idl.id
  }

  vpc_id = aws_vpc.eman.id
}

resource "aws_subnet" "subnet_10_10_10" {
  availability_zone                   = "us-west-1b"
  cidr_block                          = "10.10.10.0/24"
  map_public_ip_on_launch             = true
  private_dns_hostname_type_on_launch = "ip-name"
  vpc_id                              = aws_vpc.eman.id
}

resource "aws_subnet" "subnet_10_10_11" {
  availability_zone                   = "us-west-1c"
  cidr_block                          = "10.10.11.0/24"
  map_public_ip_on_launch             = true
  private_dns_hostname_type_on_launch = "ip-name"
  vpc_id                              = aws_vpc.eman.id
}

resource "aws_vpc" "eman" {
  cidr_block           = "10.10.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  instance_tenancy     = "default"
}